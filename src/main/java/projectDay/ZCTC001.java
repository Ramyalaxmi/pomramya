package projectDay;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.collections4.bag.CollectionSortedBag;
import org.bson.types.Symbol;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import net.bytebuddy.matcher.CollectionSizeMatcher;


public class ZCTC001 
{
	@Test
	public void carPrice() throws InterruptedException{
		
	
	System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
	ChromeDriver driver = new ChromeDriver();
	driver.get("https://www.zoomcar.com/chennai/");
	driver.manage().window().maximize();
	driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	driver.findElementByLinkText("Start your wonderful journey").click();
	driver.findElementByXPath("//div[@class='component-popular-locations']/div[5]").click();
	driver.findElementByXPath("//button[@class='proceed']").click();
	
	
	// Get the current date
			Date date = new Date();
	// Get only the date (and not month, year, time etc)
			DateFormat sdf = new SimpleDateFormat("dd"); 
	// Get today's date
			String today = sdf.format(date);
	// Convert to integer and add 1 to it
			int tomorrow = Integer.parseInt(today)+1;
	// Print tomorrow's date
			System.out.println(tomorrow);
	driver.findElementByXPath("//div[@class='day' and contains(text(), '"+tomorrow+"')]").click();
	
	driver.findElementByXPath("//button[@class='proceed']").click();
	Thread.sleep(3000);
	String checkdate = driver.findElementByXPath("//div[@class='day picked ']").getText();
System.out.println("Selected date is "+checkdate);
	
	if (checkdate.contains(""+tomorrow))
	{
		System.out.println("Value is same as selected");
		driver.findElementByXPath("//button[@class='proceed']").click();	
	}
	
	List<String> maxprice = new ArrayList<>();
	Thread.sleep(5000);
	List<WebElement> priceList = driver.findElementsByXPath("//div[@class='price']");
	int size = priceList.size();
	for (WebElement allPrice : priceList) {
		
		System.out.println(allPrice.getText().replaceAll("\\D", ""));
		maxprice.add(allPrice.getText().replaceAll("\\D", ""));
	}
	String maxNum = Collections.max(maxprice);
	System.out.println("Maximum price is "+maxNum);
	
	String carname;
	
	carname = driver.findElementByXPath("//div[contains(text(),'"+maxNum+"')]/preceding::h3[1]").getText();
	System.out.println("Car Name is "+carname);
	driver.findElementByXPath("//div[contains(text(),'"+maxNum+"')]/following::button[1]").click();
	
	driver.close();
	}
	
	
	
	
	
}
