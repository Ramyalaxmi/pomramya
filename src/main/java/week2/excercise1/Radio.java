package week2.excercise1;

public abstract class Radio {

	public void playStation()
	{
		System.out.println("Implemented method in Radio - Playstation");
	}
	
	public abstract void changeBattery();
	
	
}
