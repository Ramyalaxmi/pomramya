package testcases;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import LearnExcel.learnExcelintegration;
import wdMethods.ProjectMethods;

public class TC002CreateLead extends ProjectMethods{
	
	@BeforeTest
	public void setData() {
		testCaseName = "TC001CreateLead";
		testDesc = "Create A new Lead";
		author = "Jayashree";
		category = "smoke";
	}

	@Test(dataProvider="qa")
	public void createLead(String cname,String fname, String lname,String email, String ph) {		
				
		WebElement elelinktextCreateLead = locateElement("linktext", "Create Lead");
		click(elelinktextCreateLead);
		
		WebElement eleCompanyName = locateElement("id", "createLeadForm_companyName");
		type(eleCompanyName, cname);
		
		WebElement eleFirstName = locateElement("id", "createLeadForm_firstName");
		type(eleFirstName, fname);
		
		WebElement eleLastName = locateElement("id", "createLeadForm_lastName");	
		type(eleLastName, lname);
		
//		WebElement eleDropdown = locateElement("createLeadForm_dataSourceId");
//		selectDropDownUsingText(eleDropdown, "Employee");
		
//		WebElement eleFirstnameLocal = locateElement("id", "createLeadForm_firstNameLocal");
//		type(eleFirstnameLocal,"Jay");
		
//		WebElement eleLastnameLocal = locateElement("id", "createLeadForm_lastNameLocal");
//		type(eleLastnameLocal,"K");
		
//		WebElement ele_salutation = locateElement("id", "createLeadForm_personalTitle");
//		type(ele_salutation,"Mrs");
//		
//		WebElement ele_title = locateElement("id", "createLeadForm_generalProfTitle");
//		type(ele_title,"Jayashree K");
//		
//		WebElement ele_annual = locateElement("id", "createLeadForm_annualRevenue");
//		type(ele_annual,"400000");
//		
//		WebElement ele_Industry = locateElement("createLeadForm_industryEnumId");
//		selectDropDownUsingText(ele_Industry, "Computer Software");	
//		
//		WebElement ele_ownership = locateElement("createLeadForm_ownershipEnumId");
//		selectDropDownUsingText(ele_ownership, "Public Corporation");	
//		
//		WebElement ele_sic = locateElement("id", "createLeadForm_sicCode");
//		type(ele_sic,"123456");
//		
//		WebElement ele_description= locateElement("id", "createLeadForm_description");
//		type(ele_description,"Hi.This is Jayashree working with Open IT Works Solutions.");
//		
//		WebElement ele_impnote= locateElement("id", "createLeadForm_importantNote");
//		type(ele_impnote,"Important Note");
//		
//		WebElement ele_ccode= locateElement("id", "createLeadForm_primaryPhoneCountryCode");
//		type(ele_ccode,"+91");
//		
//		WebElement ele_areacode= locateElement("id", "createLeadForm_primaryPhoneAreaCode");
//		type(ele_areacode,"21");
//		
//		WebElement ele_phext= locateElement("id", "createLeadForm_primaryPhoneExtension");
//		type(ele_phext,"+91");
		
		WebElement ele_phno= locateElement("id", "createLeadForm_primaryPhoneNumber");
		type(ele_phno,ph);
		
		
		WebElement ele_email= locateElement("id", "createLeadForm_primaryEmail");
		type(ele_email,email);
		
//		WebElement ele_dept= locateElement("id", "createLeadForm_departmentName");
//		type(ele_dept	,"IT");
//		
//		WebElement ele_dropdown_currency = locateElement("createLeadForm_currencyUomId");
//		selectDropDownUsingText(ele_dropdown_currency, "INR - Indian Rupee");
		
		WebElement ele_CreateLead = locateElement("xpath", "//input[@name='submitButton']");
		click(ele_CreateLead);		
		
	}
	@DataProvider(name="qa", indices = {1})
	public Object[][] fetchData() throws IOException{
		Object[][] data =  learnExcelintegration.LearnExcel();
		return data;
		
		/*data[0][0]="TestLeaf";
		data[0][1]="Jayashree";
		data[0][2]="Subash";
		data[0][3]="kjayashree2007@gmail.com";
		data[0][4]=123456789;
		
		data[1][0]="TestLeaf";
		data[1][1]="Ramya";
		data[1][2]="Kannan";
		data[1][3]="kannan@gmail.com";
		data[1][4]=123456789;*/
		
		
	}
	
}












