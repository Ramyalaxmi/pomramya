package testcases;


import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class TC001EditLead extends ProjectMethods {
	
	@BeforeTest
	//(groups= {"any"})
	public void setData() {
		testCaseName = "TC001EditLead";
		testDesc = "Edit A Lead";
		author = "Jayashree";
		category = "smoke";
	}
	
	@Test
	//(groups= {"sanity"},dependsOnGroups={"smoke"})
	public void editlead() throws InterruptedException {

		WebElement eleleads = locateElement("xpath", "(//div[@class='x-panel-header']/a)[2]");
		click(eleleads);
		WebElement eleFindLeads = locateElement("linktext", "Find Leads");
		click(eleFindLeads);
		WebElement eleFirstname = locateElement("xpath", "(//div[@class='x-form-element'])[19]/input");
		type(eleFirstname, "Jayashree");
		WebElement eleFindLeads_button = locateElement("xpath", "(//button[@class='x-btn-text'])[7]");
		click(eleFindLeads_button);
//		WebElement ele_text = locateElement("xpath", "//table[@class='x-grid3-row-table']/tbody/tr/td/div/a");
//		getText(ele_text);
		Thread.sleep(1000);
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//table[@class='x-grid3-row-table']/tbody/tr/td/div/a"))).click();
		WebElement ele_edit = locateElement("linktext", "Edit");
		click(ele_edit);
		WebElement ele_comp_name = locateElement("id", "updateLeadForm_companyName");
		type(ele_comp_name, "Open IT");
		WebElement ele_update = locateElement("class", "smallSubmit");
		click(ele_update);
	
	
	}
	
}