package week1.exercise1;

import java.util.Scanner;

public class SwitchCaseLearning {

	public static void main(String[] args) {
		// Write a new program to take the first 3 digits of the mobile number as input and 
		//print the mobile service provider. (944 - BSNL, 900 - Airtel, 897 - Idea, 630 - JIO) 
System.out.println("hello");
		Scanner s = new Scanner(System.in);
		System.out.println("Enter the option:");
		int option = s.nextInt();
		switch (option) {
		case 944:
			System.out.println("BSNL");
			break;
		case 900:
			System.out.println("Airtel");
			break;
		case 897:
			System.out.println("Idea");
			break;
		case 630:
			System.out.println("JIO");
			break;
		default:
			System.out.println("Invalid data");
			break;
		}
	}

}
