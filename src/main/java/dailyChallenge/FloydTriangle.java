package dailyChallenge;

import java.util.Scanner;

public class FloydTriangle {

	public static void main(String[] args) {
	
		//FloydTriangle
		/*
		 1
		 2 3
		 4 5 6
		 7 8 9 10
		 */
		Scanner sc = new Scanner(System.in);
		int size = sc.nextInt();
		int num = 1;
		System.out.println("Maximum limit to be printed is "+size);
		
		for (int i = 1; i<=size; i++)
		{
			for (int j = 1; j<=i; j++)
			{
				System.out.print(num + " ");
				num++;
			}
			System.out.println();
		}
		
		

	}

}
