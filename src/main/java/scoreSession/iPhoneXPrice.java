package scoreSession;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.chrome.ChromeDriver;

public class iPhoneXPrice {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("https://www.flipkart.com/");
		driver.getKeyboard().sendKeys(Keys.ESCAPE);
		driver.manage().window().maximize();
		driver.findElementByClassName("LM6RPg").sendKeys("iphonex");
		driver.findElementByXPath("//div[@class='col-1-12']/button").click();
		String price = driver.findElementByXPath("//div[@class='_1vC4OE _2rQ-NK']").getText();
		System.out.println("Price of 2nd iphone: "+price);
		
		
		
	}

}
