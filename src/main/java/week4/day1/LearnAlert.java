package week4.day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearnAlert {

	public static void main(String[] args) throws InterruptedException {

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		
		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		driver.switchTo().frame("iframeResult");
		driver.findElementByXPath("//button[text()='Try it']").click();
		Thread.sleep(1000);
		
		Alert alertRef = driver.switchTo().alert();
		String text = alertRef.getText();
		System.out.println("Text in alert popup is: "+text);
		alertRef.sendKeys("textttt");
		alertRef.accept();
		
		String text2 = driver.findElementById("demo").getText();
		System.out.println(text2);
		
		if(text2.contains("Ramya"))
		{
			System.out.println("Ramya exists");
		}
		else
		{
			System.out.println("The text does not exists");
		}
		
		
		

	}

}
